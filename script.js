let num = 2;
let getCube = num ** 3;
console.log(`The cube of ${num} is ${getCube}`);

let address = ['258 Washington Ave', 'NW', 'California', '90011'];
let [street, city, state, zip] = address;

console.log(`I live at ${street} ${city}, ${state} ${zip}`);

let animal = {
    name: 'Lolong',
    type: 'saltwater crocodile',
    heightFeet: 20,
    heightInch: 3,
    weightKg: 1075,
}

let { name, type, heightFeet, heightInch, weightKg } = animal;

console.log(`${name} was a ${type}. It weighed at ${weightKg} kgs with a measurement of ${heightFeet} ft ${heightInch} in.`)

let numbers = [1, 2, 3, 4, 5];

numbers.forEach(e => console.log(e));
let reduceNumber = numbers.reduce((acc, curr) => acc + curr, 0);
console.log(reduceNumber);

class Dog {
    constructor(name, age, breed) {
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}

let breedOne = new Dog();
breedOne.name = 'Frankie';
breedOne.age = 5;
breedOne.breed = 'Miniature Dachshund';
console.log(breedOne);